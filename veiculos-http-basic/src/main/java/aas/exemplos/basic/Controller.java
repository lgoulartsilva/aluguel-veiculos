package aas.exemplos.basic;

import java.net.URI;

import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.hateoas.Resource;
import org.springframework.hateoas.Resources;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;

@AllArgsConstructor
@RestController
public class Controller {
	
	private final Repositorio repositorio;
	private final Assembler assembler;
	
	@GetMapping("/veiculo")
	public Resources<Resource<Veiculo>> recuperaVeiculos() {
		return assembler.toResources(repositorio.findAll());
	}
	
	@GetMapping("/veiculo/{idVeiculo}")
	public Resource<Veiculo> recuperaVeiculo(@PathVariable Long idVeiculo) {
		Veiculo veiculo = repositorio.findById(idVeiculo)
				.orElseThrow(() -> new ResourceNotFoundException(String.format("Não foi possível encontrar o veículo com id %d", idVeiculo)));
		
		return assembler.toResource(veiculo);
	}
	
	@PostMapping("/veiculo")
	public ResponseEntity<Resource<Veiculo>> cadastraVeiculo(@RequestBody Veiculo veiculo) {
		Resource<Veiculo> resource = assembler.toResource(repositorio.save(veiculo));
		return ResponseEntity.created(URI.create(resource.getId().expand().getHref())).body(resource);
	}
	
	@PutMapping("/veiculo/{idVeiculo}")
	public ResponseEntity<Resource<Veiculo>> atualizaVeiculo(@RequestBody Veiculo veiculo, @PathVariable Long idVeiculo) {
		return repositorio.findById(idVeiculo).map(veiculoRecuperado -> {
			veiculoRecuperado.setMarca(veiculo.getMarca());
			veiculoRecuperado.setModelo(veiculo.getModelo());
			veiculoRecuperado.setPlaca(veiculo.getPlaca());
			return ResponseEntity.ok(assembler.toResource(repositorio.save(veiculoRecuperado)));
		}).orElse(cadastraVeiculo(veiculo));
	}
	
	@DeleteMapping("/veiculo/{idVeiculo}")
	public <T> ResponseEntity<T> excluiVeiculo(@PathVariable Long idVeiculo) {
		repositorio.deleteById(idVeiculo);
		return ResponseEntity.noContent().build();
	}
}
