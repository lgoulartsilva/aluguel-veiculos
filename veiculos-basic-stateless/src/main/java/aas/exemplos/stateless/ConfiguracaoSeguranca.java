package aas.exemplos.stateless;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

@Configuration
@EnableWebSecurity
public class ConfiguracaoSeguranca extends WebSecurityConfigurerAdapter {

	private static final String ESCRITA = "ESCRITA";
	private static final String LEITURA = "LEITURA";
	private static final String VEICULO_URL = "/veiculo/**";

	@Override
	protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
		
		auth.inMemoryAuthentication()
				.withUser("admin").password(encoder().encode("admin")).authorities(LEITURA, ESCRITA).and()
				.withUser("simples").password(encoder().encode("simples")).authorities(LEITURA);
	}

	@Override
	protected void configure(final HttpSecurity http) throws Exception {
		http.csrf().disable()
				.authorizeRequests()
				.antMatchers(HttpMethod.GET, VEICULO_URL).hasAuthority(LEITURA)
				.antMatchers(HttpMethod.PUT, VEICULO_URL).hasAuthority(ESCRITA)
				.antMatchers(HttpMethod.POST, VEICULO_URL).hasAuthority(ESCRITA)
				.antMatchers(HttpMethod.DELETE, VEICULO_URL).hasAuthority(ESCRITA)
				.and().httpBasic()
				.and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
}
