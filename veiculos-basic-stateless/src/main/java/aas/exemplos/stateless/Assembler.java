package aas.exemplos.stateless;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.hateoas.Resource;
import org.springframework.hateoas.ResourceAssembler;
import org.springframework.hateoas.Resources;
import org.springframework.stereotype.Component;

@Component
public class Assembler implements ResourceAssembler<Veiculo, Resource<Veiculo>> {

	private static final String REL = "veiculos";

	@Override
	public Resource<Veiculo> toResource(Veiculo veiculo) {

		return new Resource<>(veiculo,
				linkTo(methodOn(Controller.class).recuperaVeiculo(veiculo.getId())).withSelfRel(),
				linkTo(methodOn(Controller.class).recuperaVeiculos()).withRel(REL));
	}

	public Resources<Resource<Veiculo>> toResources(List<Veiculo> veiculos) {

		return new Resources<>(veiculos.stream().map(this::toResource).collect(Collectors.toList()),
				linkTo(methodOn(Controller.class).recuperaVeiculos()).withSelfRel());
	}
}
