package aas.exemplos.stateless;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Data;

@Data
@Entity
public class Veiculo {
	@Id
	@GeneratedValue
	private Long id;
	private String marca;
	private String modelo;
	private String placa;
}
