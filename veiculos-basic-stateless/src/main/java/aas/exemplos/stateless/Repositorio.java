package aas.exemplos.stateless;

import org.springframework.data.jpa.repository.JpaRepository;

public interface Repositorio extends JpaRepository<Veiculo, Long> {
}
